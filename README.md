# Obantoo

Fork of http://obantoo.sourceforge.net/

## Usage

Gradle
```groovy
repositories {
    maven {  url "https://michaelsp.github.io/maven-repo" }
}

dependency {
    implementation "com.github.michaelsp:obantoo:2.1.12"
}
```

Maven
```xml
<repositories>
    <repository>
        <id>michaelsp</id>
        <url>https://michaelsp.github.io/maven-repo</url>
    </repository>
</repositories>

<dependencies>
    <dependency>
      <groupId>com.github.michaelsp</groupId>
      <artifactId>obantoo</artifactId>
      <version>2.1.12</version>
    </dependency>
</dependencies>
```
